# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('vendor/startbootstrap-freelancer')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w(freelancer.css freelancer.js contact_me.js jqBootstrapValidation.js mail.js
                                                 bootstrap/js/* fontawesome-free/css/* fontawesome-free/webfonts/* jquery/* jquery-easing/*)
