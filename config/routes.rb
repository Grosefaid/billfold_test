Rails.application.routes.draw do
  root 'home#index'
  post '/send', to: 'mail#contact_me'
end
