class MessageMailer < ApplicationMailer
  def new_message
    @message = params[:message]
    mail(subject: 'New message!')
  end
end