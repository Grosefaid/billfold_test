class ApplicationMailer < ActionMailer::Base
  default from: 'siyalovivan@gmail.com'
  default to: 'grosefaid@gmail.com'
  layout 'mailer'
end
