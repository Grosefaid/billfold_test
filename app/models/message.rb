class Message < ApplicationRecord
  after_create :send_mail

  def send_mail
    MessageMailer.with(message: self).new_message.deliver_later
  end
end