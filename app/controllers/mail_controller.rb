class MailController < ApplicationController
  def contact_me
    Message.create(message_params)
  end

  private

  def message_params
    params.permit(:name, :phone, :email, :message)
  end
end
